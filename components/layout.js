import Link from 'next/link';
import Image from 'next/image'

export default function Layout({ children }) {
  return (
    <div className='flex flex-col min-h-screen'>
      <header className='bg-fuchsia-100 mb-8 py-4'>
        <div className='container mx-auto flex justify-center'>
          <Link href='/'>
            <a>🏡</a>
          </Link>
          &nbsp;
          <Link href='/about'>
            <a>❓</a>
          </Link>
          <span className='mx-auto'>‧͙⁺˚*･༓☾　����.𝚔𝚌𝚊𝚛𝚝𝚙.𝚕𝚊　☽༓･*˚⁺‧͙</span>{' '}
        </div>
      </header>
      <main className='container mx-auto flex-1'>
      <Image width="64" height="64" src="/images/ckanime.gif" />
        {children}
      </main>
      <footer className='bg-fuchsia-100 mt-8 py-4'>
        <div className='container mx-auto flex justify-center'>
        -ˏ͛⑅　‧̥̥͙‧̥̥ ̥ ̮ ̥ ⊹ ‧̫‧ ⊹ ̥ ̮ ̥ ‧̥̥‧̥̥͙　⑅ˏ͛-
        </div>
      </footer>
    </div>
  );
}