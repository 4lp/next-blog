import Layout from '../components/layout';
import '../styles/globals.css';
import Image from 'next/image'

function MyApp({ Component, pageProps }) {
  return (
    <div>

    <Layout>
      <Component {...pageProps} />
    </Layout>
    </div>
  );
}
export default MyApp;