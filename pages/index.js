import fs from 'fs';
import matter from 'gray-matter';
import Image from 'next/image';
import Link from 'next/link';

export async function getStaticProps() {
  const files = fs.readdirSync('posts');
  const posts = files.map((fileName) => {
    const slug = fileName.replace('.md', '');
    const readFile = fs.readFileSync(`posts/${fileName}`, 'utf-8');
    const { data: frontmatter } = matter(readFile);
    
    return {
      slug,
      frontmatter,
    };
  });

  return {
    props: {
      posts,
    },
  };
}

export default function Home({ posts }) {
  return (
    <div className='grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 p-4 md:p-0'>
      <ul className='list-disc'>
      {posts.map(({ slug, frontmatter }) => (
        <li
          key={slug}
          className='marker:text-sky-400 hover:marker:text-fuchsia-400'
        >
          <Link href={`/post/${slug}`} className='cursor-pointer'>
            <a>
              <h1 className='p-4'>{frontmatter.title}</h1>
            </a>
          </Link>
          <p className='text-xs'>
                date: {frontmatter.date}
                <br />
                tags: {frontmatter.tags}
              </p>
        </li>
      ))}
      </ul>
    </div>
  );
}