import Link from 'next/link';
import Image from 'next/image'

export default function About({ children }) {
  return (
    <div className='flex flex-col min-h-screen'>
      <h1 className='p-4'><em>???</em></h1>
      <p>
        a place to record programming things I don't want to forget + anything cool I find
        <br />
        <br />
        a playground for design
        <br />
        <br />
        this is mostly just for me.
        <br />
        <br />
        <br />
        <br />
      </p>
      <h1 className='p-4'><em>who?</em></h1>
      <p>
        a girl who's into:
      </p>
      <ul className='list-disc list-inside'>
          <li>weird music</li>
          <li>djing</li>
          <li>computers</li>
          <li>web stuff</li>
          <li>novels</li>
          <li>anime, manga, visual novels &etc.</li>
          <li></li>
          <li>esotericism and errata</li>
          <li>the detritus of life</li>
      </ul>
      <br />
        <br />
        <br />
        <br />
      <h1 className='p-4'><em>how?</em></h1>
      <p>
        next.js and tailwind (*｀･へ･´*)
        <br />
        <br />
        thanks to <a href="https://blog.openreplay.com/creating-a-markdown-blog-powered-by-next-js-in-under-an-hour" target="blank">this</a> tutorial
      </p>
    </div>
  );
}