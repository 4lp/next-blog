---
title: 'hoverable margins an for absolute positioned element'
socialImage: images/Knife_art.png
date: '2022-03-19'
tags:
  - css
---

I ran into an issue where I wanted some extra margins to activate :hover on an absolutely positioned div but the margin property doesn't affect absolutely positioned elements : / I also wanted a pure css solution if possible! finally got it by using psudeo-elements though so here's an explanation

the element I needed the hoverable margin for: 
```css
.submenu {
  display: block;
  position: absolute;
  color: #000;
  z-index: 9999;
  background: #fff;
  overflow: visible;
  left: 0;
  // bonus! we can move an absolute positioned element relative to its parent by using transform!
  transform: translateY(calc(-50% - 11px));
}
```

hoverable margin element (a bigger div behind .submenu)
```css
.submenu:before {
  content: '';
  position: absolute;
  display: none;
  // height and width are relative to parent
  width: 300px;
  height: calc(140% + 20px);
  // (height - 100%)/ 2 to center
  bottom: calc(-20% - 10px);
  left: -20px;
  // transparent black so we can toggle the last 2 values to check the element size/positioning easily
  background-color: #00000000;
  z-index: -1;
  display: inline-block;
}
```

using css shapes would probably be a cool addition + make for better ux

