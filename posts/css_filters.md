---
title: 'css filters'
socialImage: images/Knife_art.png
date: '2022-03-19'
tags:
  - css
---

# hover styles for svgs

how do we do it? I tried:
- color
- fill
- ???

the answer is `filter`!

```css
filter: invert(71%) sepia(50%) saturate(405%) hue-rotate(59deg) brightness(97%) contrast(93%);
```

`opacity` doesn't seem to work but we can use filters for that too : )

```css
filter: opacity(35%);
```

hex to filter convertor: https://codepen.io/sosuke/pen/Pjoqqp